package com.example.routinely

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_timer.*

import kotlinx.android.synthetic.main.content_timer.*

class TimerActivity : AppCompatActivity() {

    val timers: ArrayList<Timer> = ArrayList()

    private lateinit var currentTimer: CountDownTimer
    private lateinit var totalTimer: CountDownTimer
    private var timerLengthSeconds: Long = 0

    private var secondsRemaining: Long = 0

    private var totalSecondsRemaining:Long =0

    private lateinit var runningTimer: Timer
    private var runningTimerPosition: Int =0

    private var paused:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer)

        addTimers()

        timer_list.layoutManager = LinearLayoutManager(this)

        timer_list.adapter = RecyclerAdapter(timers, this)

        timer_list.itemAnimator.changeDuration = 0


        fab_play.setOnClickListener{_ ->
            startRoutine()
            updateButtons()
        }

        fab_stop.setOnClickListener{_ ->
            cancelTimers()
            resetTimer()
            resetButtons("reset")
        }

        fab_next.setOnClickListener {_ ->
            skipToNextTimer()
        }

        fab_pause.setOnClickListener {_ ->
            pauseTimer()
        }

        add30sec.setOnClickListener {_ ->
            addTime(30)
        }

        add1min.setOnClickListener {_ ->
            addTime(1*60)
        }

        add5min.setOnClickListener {_ ->
            addTime(5*60)
        }

        val totalDuration = getTotalDuration()
        total_time_remaining.text = "$totalDuration:00"

        val firstDuration = timers[0].duration
        time_remaining.text = "$firstDuration:00"

        fab_next.isEnabled = false
        fab_stop.isEnabled = false
    }

    private fun updateButtons() {
        fab_play.visibility= View.GONE
        fab_pause.visibility= View.VISIBLE
        fab_next.isEnabled = true
        fab_stop.isEnabled = true
    }

    private fun resetButtons(context:String) {
        fab_play.visibility= View.VISIBLE
        fab_pause.visibility= View.GONE

        if(context == "reset"){
            fab_next.isEnabled = false
            fab_stop.isEnabled = false
        }
    }

    private fun addTimers() {
        timers.add(Timer("Get out of bed", 2, description = "Get your day started right now.", minutesLeft = 2, secondsInMinuteLeft =  0, running = false, completed = false))
        timers.add(Timer("Make your bed", 3, description = "Accomplish your first task of the day.", minutesLeft = 3, secondsInMinuteLeft =  0, running = false, completed = false))
        timers.add(Timer("Meditate", 10, description = "Focus on your breath.", minutesLeft = 10, secondsInMinuteLeft = 0, running = false, completed = false))
        timers.add(Timer("Exercise", 5, description = "Do at least 10 reps of something.", minutesLeft = 5, secondsInMinuteLeft = 0, running = false, completed = false))
        timers.add(Timer("Make Tea", 5, description = "Make some tea to enjoy while you journal.", minutesLeft = 5, secondsInMinuteLeft = 0, running = false, completed = false))
        timers.add(Timer("Journal", 5, description = "What are you grateful for? What would make today great?", minutesLeft = 5, secondsInMinuteLeft = 0, running = false, completed = false))
    }

    private fun startRoutine(){
        if(!paused){
            runningTimer = timers[0]
            totalSecondsRemaining =getTotalDuration() * 60L
            secondsRemaining = runningTimer.duration *60L
            startTimer()
            startTotalTimer()
        }else {
            startTotalTimer()
            startTimer()
            paused=false
        }
    }

    private fun startTimer(){
        runningTimer.running = true
        item_description.text = runningTimer.description
        timer_list.adapter.notifyItemChanged(runningTimerPosition)
        timer_list.layoutParams.height = 700
        timer_list.translationY = 300f
        overlay.visibility = View.VISIBLE

        currentTimer = object : CountDownTimer(secondsRemaining * 1000.toLong(), 1000) {
            override fun onFinish() = onTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                val minutesRemaining = secondsRemaining / 60
                val secondsInMinuteRemaining = secondsRemaining - minutesRemaining * 60
                runningTimer.minutesLeft = minutesRemaining
                runningTimer.secondsInMinuteLeft = secondsInMinuteRemaining
                timer_list.adapter.notifyItemChanged(runningTimerPosition);
                time_remaining.text = "$minutesRemaining:${if (secondsInMinuteRemaining >= 10) secondsInMinuteRemaining else "0$secondsInMinuteRemaining"}"
            }
        }.start()
    }

    private fun getTotalDuration():Int{
        var totalDuration=0
        for(timer in timers){
            totalDuration += timer.duration
        }
        return totalDuration
    }

    private fun startTotalTimer() {

        totalTimer = object : CountDownTimer(totalSecondsRemaining * 1000.toLong(), 1000) {
            override fun onFinish() = onTotalTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                totalSecondsRemaining = millisUntilFinished / 1000
                val minutesRemaining = totalSecondsRemaining / 60
                val secondsInMinuteRemaining = totalSecondsRemaining - minutesRemaining * 60
                total_time_remaining.text = "$minutesRemaining:${if (secondsInMinuteRemaining >= 10) secondsInMinuteRemaining else "0$secondsInMinuteRemaining"}"
            }
        }.start()
    }

    private fun cancelTimers() {
        currentTimer.cancel();
        totalTimer.cancel();
        finishAllTimers()
        onTotalTimerFinished()
    }

    private fun onTotalTimerFinished() {
        val totalDuration = getTotalDuration()
        total_time_remaining.text = "$totalDuration:00"
    }

    private fun onTimerFinished() {
        runningTimer.running = false
        runningTimer.completed = true
        timer_list.adapter.notifyItemChanged(runningTimerPosition);
        checkNextTimer();
    }

    private fun finishAllTimers(){
        resetButtons("reset")
        resetTimer()

    }

    private fun skipToNextTimer(){
        if(paused){
            paused=false
            updateButtons()
        }
        totalSecondsRemaining-=secondsRemaining
        currentTimer.cancel()
        onTimerFinished()
        totalTimer.cancel()
        startTotalTimer()
    }

    private fun pauseTimer(){
        currentTimer.cancel()
        totalTimer.cancel()
        resetButtons("pause")
        paused=true;
    }

    private fun addTime(timeToAdd:Long){
        currentTimer.cancel()
        totalTimer.cancel()

        totalSecondsRemaining += timeToAdd
        secondsRemaining += timeToAdd

        val minutesRemaining = secondsRemaining / 60
        val secondsInMinuteRemaining = secondsRemaining - minutesRemaining * 60
        runningTimer.minutesLeft = minutesRemaining
        runningTimer.secondsInMinuteLeft = secondsInMinuteRemaining
        timer_list.adapter.notifyItemChanged(runningTimerPosition)

        startTotalTimer()
        startTimer()
    }

    private fun checkNextTimer() {
        if(timers.size==runningTimerPosition+1){
            resetButtons("reset")
            resetTimer()
        }else{
            runningTimerPosition ++
            runningTimer = timers[runningTimerPosition]
            timer_list.adapter.notifyItemChanged(runningTimerPosition)
            secondsRemaining = runningTimer.duration*60L
            startTimer()
        }
    }

    private fun resetTimer() {
        runningTimer = timers[0]
        time_remaining.text = "${runningTimer.duration}:00"
        runningTimerPosition = 0
        item_description.text = ""
        timer_list.layoutParams.height = 1180
        timer_list.translationY = 0f
        overlay.visibility = View.GONE
        for ((idx, timer) in timers.withIndex()){
            timer.running = false
            timer.completed = false
            timer_list.adapter.notifyItemChanged(idx)
        }
    }
}

data class Timer(val name: String, val duration: Int, val description: String, var minutesLeft:Long, var secondsInMinuteLeft:Long, var running: Boolean, var completed: Boolean)
