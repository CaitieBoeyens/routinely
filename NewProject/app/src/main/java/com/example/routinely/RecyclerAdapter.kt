package com.example.routinely

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.provider.ContactsContract
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.recyclerview_item_row.view.*


class RecyclerAdapter(val items : ArrayList<Timer>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_item_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.TimerName?.text = items[position].name
        val minutesLeft =items[position].minutesLeft.toString()
        val secondsLeft =items[position].secondsInMinuteLeft.toString()
        holder?.TimerDuration?.text = when {
            items[position].running -> "$minutesLeft:${if (secondsLeft.length == 2) secondsLeft else "0$secondsLeft"}"
            else -> items[position].duration.toString() + ":00"
        }
        holder?.TimerBlock?.setCardBackgroundColor(
            when {
                items[position].running -> ContextCompat.getColor( context, R.color.colorAccent)
                items[position].completed -> ContextCompat.getColor( context, R.color.colorPrimaryDark)
                else -> ContextCompat.getColor( context, R.color.colorPrimary)
            }
        )

        val padding = if(items[position].running) 50 else 30
        holder?.TimerBlock?.setContentPadding( padding, padding, padding, padding);
        holder?.TimerBlock?.cardElevation = ( if(items[position].completed) 0f else 4f);
        holder?.TimerBlock?.visibility=when {
            items[position].completed -> View.GONE
            else -> View.VISIBLE
        }


        holder?.TimerNext?.text = "Next:"
        holder?.TimerNext?.visibility = when {
            items[position].running && position < 5 -> View.VISIBLE
            else -> View.GONE
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

    val TimerName = view.timer_name
    val TimerNext = view.timer_next
    val TimerDuration = view.timer_duration
    val TimerBlock = view.timer_block
}



