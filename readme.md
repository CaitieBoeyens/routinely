# :clock2: Routinely

---

## About

Routinely is a simple routine tracking app. It uses Tim Ferris's morning routine as inspiration as he claims much of his success is due to his morning routine. many people find it difficult to create a morning routine and stick to it. Routinely makes it easy as there is no set up required.

[Video Demo](https://drive.google.com/file/d/1-en7MoGHBC-RELdchqhrxBpoSAwl6sUx/view?usp=sharing)

---

## Features

- **Running through routine** 
    - when you start a routine it will run through the tasks automatically starting the next task when the previous one ended
- **Add time to current task** 
- **Skip current task**
- **Pause routine**

## Coming Soon

- **Notifications**
    - You will be notified when the current task is over and what the next task is 
- **Timer persists**
    - The timer runs in the background even if the app is closed 
- **Wake up alarm**
    - You can choose to set a wakeup alarm. If this is chosen as soon as you dismiss the alarm your routine will start.

  
---

Presentation can be find [here](https://docs.google.com/presentation/d/1QZihqctP1T55259H3iK0BIA0bRCxZNTVNC7Ga_9wip4/edit?usp=sharing)

Background illustration in app and presentation from [rawpixel.com - www.freepik.com](https://www.freepik.com/free-photos-vectors/nature)